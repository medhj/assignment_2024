package ma.nemo.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductReturnDTO {
    private String productCode;
    private int quantity;
    private String reason;
}
