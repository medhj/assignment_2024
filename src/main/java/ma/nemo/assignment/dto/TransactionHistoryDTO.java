package ma.nemo.assignment.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.User;

import java.util.Date;

@Data
@AllArgsConstructor @NoArgsConstructor
public class TransactionHistoryDTO {
    private Long transactionId;
    private Product product;
    private Integer quantity;
    private String transactionType;

    private Date transactionDate;

    private User user;
}