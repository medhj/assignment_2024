package ma.nemo.assignment.dto;

import lombok.Data;

@Data
public class SaleDTO {
    private  String productCode;
    private Integer quantity;
}
