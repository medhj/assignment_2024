package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductReturnDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductReturnMapper {
    private static final ModelMapper modelMapper = new ModelMapper();
    public static ProductReturnDTO toDTO(Product product){
        return modelMapper.map(product, ProductReturnDTO.class);
    }
    public static Product toEntity(ProductReturnDTO productReturnDTO){
        return modelMapper.map(productReturnDTO, Product.class);
    }
}
