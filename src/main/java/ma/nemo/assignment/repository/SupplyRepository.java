package ma.nemo.assignment.repository;

import ma.nemo.assignment.domain.Supply;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplyRepository extends JpaRepository<Supply, Long> {
}
