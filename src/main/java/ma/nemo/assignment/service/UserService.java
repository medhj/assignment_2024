package ma.nemo.assignment.service;

import ma.nemo.assignment.domain.User;

import java.util.List;

public interface UserService {
    List<User> listUsers();
}
