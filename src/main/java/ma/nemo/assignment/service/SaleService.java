package ma.nemo.assignment.service;

import ma.nemo.assignment.domain.Sale;
import ma.nemo.assignment.dto.SaleDTO;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.ProductValidationException;


public interface SaleService {

    Sale createSale(SaleDTO saleDto) throws ProductNotFound, ProductValidationException;
}
