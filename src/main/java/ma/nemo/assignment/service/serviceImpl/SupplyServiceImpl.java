package ma.nemo.assignment.service.serviceImpl;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.dto.SupplyDTO;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.mapper.SupplyMapper;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.SupplyRepository;
import ma.nemo.assignment.service.SupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplyServiceImpl implements SupplyService {
    private final SupplyRepository supplyRepository;
    private final ProductRepository productRepository;
    private final SupplyMapper supplyMapper;

    @Autowired
    public SupplyServiceImpl(SupplyRepository supplyRepository, ProductRepository productRepository, SupplyMapper supplyMapper) {
        this.supplyRepository = supplyRepository;
        this.productRepository = productRepository;
        this.supplyMapper = supplyMapper;
    }


    @Override
    public void addSupply(SupplyDTO supplyDTO) throws ProductNotFound {

        Product product = productRepository.findByProductCode(supplyDTO.getProductCode());
        if(product == null) {
            throw new ProductNotFound("Product with code " + supplyDTO.getProductCode() + " not found.");
        }
        if(supplyDTO.getQuantity() > 500){
            throw new IllegalArgumentException("Quantity exceeds the maximum allowed (500).");
        }
        int quantity = supplyDTO.getQuantity();
        int currentQuantity = product.getQuantityInStock();
        int newQuantity = currentQuantity +quantity;
        product.setQuantityInStock(newQuantity);

        Supply supply= supplyMapper.toEntity(supplyDTO);
        supply.setProduct(product);
        supply.setQuantity(newQuantity);
        supplyRepository.save(supply);

    }
}
