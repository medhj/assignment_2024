package ma.nemo.assignment.service.serviceImpl;

import ma.nemo.assignment.dto.ProductReturnDTO;
import ma.nemo.assignment.dto.TransactionHistoryDTO;
import ma.nemo.assignment.mapper.TransactionHistoryMapper;
import ma.nemo.assignment.repository.TransactionHistoryRepository;
import ma.nemo.assignment.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.exceptions.ProductAlreadyExists;
import ma.nemo.assignment.exceptions.ProductValidationException;
import ma.nemo.assignment.repository.ProductRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final TransactionHistoryRepository transactionHistoryRepository;
    private final TransactionHistoryMapper transactionHistoryMapper;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, TransactionHistoryRepository transactionHistoryRepository, TransactionHistoryMapper transactionHistoryMapper) {
        this.productRepository = productRepository;
        this.transactionHistoryRepository = transactionHistoryRepository;
        this.transactionHistoryMapper = transactionHistoryMapper;
    }

    @Override
    public Product createProduct(Product product) throws ProductAlreadyExists, ProductValidationException {
        if (productRepository.findByProductCode(product.getProductCode()) != null) {
            throw new ProductAlreadyExists("Product with code " + product.getProductCode() + " already exists");
        }



        Date currentDate = new Date();
        product.setCreationDate(currentDate);
        product.setModificationDate(currentDate);

        Product savedProduct = productRepository.save(product);

        return savedProduct;
    }

    @Override
    public List<Product> listProducts() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> getProductById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public boolean deleteProduct(Long id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            productRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Product> getProductsBelowThreshold() {
        return productRepository.getProductsBelowThreshold();
    }

    @Override
    public void setThreshold(String productCode, int thresholdQuantity) {
        Product product = productRepository.findByProductCode(productCode);
        if (product != null) {
            product.setThresholdQuantity(thresholdQuantity);
            productRepository.save(product);
        }
    }




    @Override
    public List<Product> findProductsNearingExpiry(Date currentDate) {
        return productRepository.findProductsNearingExpiry(currentDate);
    }

    @Override
    public void returnProduct(ProductReturnDTO productReturnDTO) {
        Product product= productRepository.findByProductCode(productReturnDTO.getProductCode());
        if(product!=null){
            int currentStock = product.getQuantityInStock();
            int returnQuantity = productReturnDTO.getQuantity();
            product.setQuantityInStock(currentStock+returnQuantity);
            TransactionHistoryDTO returnTransaction = new TransactionHistoryDTO();
            returnTransaction.setProduct(product);
            returnTransaction.setQuantity(returnQuantity);
            returnTransaction.setTransactionType("RETURN");
            returnTransaction.setTransactionDate(new Date());
            transactionHistoryRepository.save(transactionHistoryMapper.toEntity(returnTransaction));


        }
    }

}
