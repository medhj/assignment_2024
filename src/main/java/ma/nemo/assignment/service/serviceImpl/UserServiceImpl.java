package ma.nemo.assignment.service.serviceImpl;

import ma.nemo.assignment.service.UserService;
import org.springframework.stereotype.Service;
import ma.nemo.assignment.domain.User;
import ma.nemo.assignment.repository.UserRepository;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> listUsers() {
        return userRepository.findAll();
    }
}
