package ma.nemo.assignment.service.serviceImpl;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Sale;
import ma.nemo.assignment.dto.SaleDTO;
import ma.nemo.assignment.repository.SaleRepository;
import ma.nemo.assignment.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.ProductValidationException;
import ma.nemo.assignment.mapper.SaleMapper;

import ma.nemo.assignment.repository.ProductRepository;


@Service
public class SaleServiceImpl implements SaleService {

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SaleMapper saleMapper;

    @Override
    public Sale createSale(SaleDTO saleDto) throws ProductNotFound, ProductValidationException {
        // Retrieve the product based on the product code from saleDto
        Product product = productRepository.findByProductCode(saleDto.getProductCode());

        if (product != null) {
            if (product.getQuantityInStock() >= saleDto.getQuantity()) {
                // Reduce the quantity in stock
                int remainingQuantity = product.getQuantityInStock() - saleDto.getQuantity();
                product.setQuantityInStock(remainingQuantity);

                // Save the updated product
                productRepository.save(product);

                // Create and save the sale
                Sale sale = saleMapper.saleDtoToSale(saleDto);
                sale.setProduct(product);
                return saleRepository.save(sale);
            } else {
                throw new ProductValidationException("Not enough quantity in stock");
            }
        } else {
            throw new ProductNotFound("Product not found");
        }
    }
}
