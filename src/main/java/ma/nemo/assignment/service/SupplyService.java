package ma.nemo.assignment.service;

import ma.nemo.assignment.dto.SupplyDTO;
import ma.nemo.assignment.exceptions.ProductNotFound;
import org.springframework.stereotype.Service;

@Service
public interface SupplyService {
    void addSupply(SupplyDTO supplyDTO) throws ProductNotFound;
}
