package ma.nemo.assignment.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import lombok.Data;
import org.hibernate.validator.constraints.Length;


import javax.validation.constraints.*;


@Entity
@Table(name = "Products")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long productId;

  @Column(unique = true, nullable = false)
  @NotBlank(message = "Product code is required")
  @Length(min = 3, max = 10, message = "Product code must be between 3 and 10 characters")
  private String productCode;

  @Column(nullable = false)
  @NotBlank(message = "Product name is required")
  private String productName;

  private String description;

  @NotNull(message = "Unit price is required")
  @DecimalMin(value = "0.0", inclusive = false, message = "Unit price must be greater than 0")
  private BigDecimal unitPrice;

  @NotNull(message = "Quantity in stock is required")
  @Min(value = 1, message = "Quantity in stock must be at least 1")
  private Integer quantityInStock;


  @NotNull(message = "Threshold quantity is required")
  @Min(value = 1, message = "Threshold quantity must be at least 1")
  private Integer thresholdQuantity;




  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Date modificationDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Date expirationDate;


}
