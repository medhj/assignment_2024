package ma.nemo.assignment.web;

import ma.nemo.assignment.dto.ThresholdRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.exceptions.ProductAlreadyExists;
import ma.nemo.assignment.exceptions.ProductValidationException;
import ma.nemo.assignment.service.ProductService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("products")
    public ResponseEntity<Long> createProduct(@Valid @RequestBody Product product)
            throws ProductAlreadyExists, ProductValidationException {
        LOGGER.info("Creating product: {}", product);

        Product saved = productService.createProduct(product);
        return new ResponseEntity<>(saved.getProductId(), HttpStatus.CREATED);
    }

    @GetMapping("products/list")
    public ResponseEntity<List<Product>> listProducts() {
        LOGGER.info("Listing products");
        List<Product> products = productService.listProducts();

        if (CollectionUtils.isEmpty(products)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
    }

    @GetMapping("products/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable Long id) {
        Optional<Product> optionalProduct = productService.getProductById(id);
        if (optionalProduct.isPresent()) {
            LOGGER.info("Product with id {} found", id);
            return new ResponseEntity<>(optionalProduct.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        boolean deleted = productService.deleteProduct(id);
        if (deleted) {
            LOGGER.info("Product with id {} deleted", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            LOGGER.error("Product with id {} not found for deletion", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/threshold-alerts")
    public ResponseEntity<List<Product>> getProductsBelowThreshold() {
        LOGGER.info("Fetching products below threshold");
        List<Product> products = productService.getProductsBelowThreshold();
        if (CollectionUtils.isEmpty(products)) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
    }

    @PostMapping("/set-threshold")
    public ResponseEntity<String> setThreshold(@RequestBody ThresholdRequest thresholdRequest) {
        LOGGER.info("Setting threshold for product: {}", thresholdRequest.getProductCode());
        productService.setThreshold(thresholdRequest.getProductCode(), thresholdRequest.getThresholdQuantity());
        return new ResponseEntity<>("Threshold set successfully", HttpStatus.OK);
    }


}
