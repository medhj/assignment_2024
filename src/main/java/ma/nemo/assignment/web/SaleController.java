package ma.nemo.assignment.web;

import ma.nemo.assignment.domain.Sale;
import ma.nemo.assignment.dto.SaleDTO;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.ProductValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import ma.nemo.assignment.service.SaleService;

@RestController
@RequestMapping("/api/sale")
public class SaleController {

    @Autowired
    private SaleService saleService;

    @PostMapping
    public ResponseEntity<?> createSale(@RequestBody SaleDTO saleDto) {
        try {
            Sale sale = saleService.createSale(saleDto);
            return ResponseEntity.ok(sale);
        } catch (ProductNotFound e) {
            // Handle the ProductNotFound exception, return an appropriate response or error message.
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body( e.getMessage());
        } catch (ProductValidationException e) {
            // Handle the ProductValidationException, return an appropriate response or error message.
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}

