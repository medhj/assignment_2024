package ma.nemo.assignment.web;


import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/expiry-alerts")
public class ExpiryAlertController {

    private final ProductService productService;

    public ExpiryAlertController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> getExpiryAlerts() {
        Date currentDate = new Date();
        List<Product> productsNearingExpiry = productService.findProductsNearingExpiry(currentDate);

        if (productsNearingExpiry.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(productsNearingExpiry, HttpStatus.OK);

        }
    }
}