package ma.nemo.assignment.web;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductReturnDTO;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/return")
public class ProductReturnController {
    private final ProductService productService;
    private final ProductRepository productRepository;

    public ProductReturnController(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;
    }

    @PostMapping
    public ResponseEntity<?> returnProduct(@RequestBody ProductReturnDTO productReturnDTO){
        try {
            if(productReturnDTO == null || productReturnDTO.getProductCode()== null || productReturnDTO.getQuantity() == 0){
                return ResponseEntity.badRequest().body("Invalid Return Request.  Please provide product code and quantity.");
            }
            Product product = productRepository.findByProductCode(productReturnDTO.getProductCode());
            if(product== null){
                return ResponseEntity.badRequest().body("Product not found.");
            }
            productService.returnProduct(productReturnDTO);
            return ResponseEntity.ok("Product return successful.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred during product return.");
        }
    }
}

